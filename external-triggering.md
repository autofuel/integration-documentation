# External triggering

## Events

All events should be published to the topic named `robot-event` , for example:

```json
TopicArn: "arn:aws:sns:eu-west-1:719432241830:robot-event"
```

The `Message` content must be a JSON-object containing all the data that is found relevant for each case - in addition to any required fields listed for a specific event.

### Status

#### Session specific

* `vehicle_detected` - a status that is published when a vehicle is detected. This can be triggered by external systems, based on license plate cameras, refueling requests from an app or simular. When sending this to the cloud system, a new UUID based `transactionId` should be generated and sent in the message attributes, like shown below.

```json
{
  "side": "left", // the side of the robot, where the car has arrived. 'left' or 'right'.
  "licensePlate": "BP30963", 
  "countryCode": "DK" //  ISO 3166 Alpha 2
}
```

## Examples

### Car is detected

Sent to topic: `robot-event`

```json
{
  "MessageAttributes": {
    "entityId": "TEST_ROBOT_1",
    "entityType": "robot",
    "messageType": "event",
    "valueType": "status",
    "value": "vehicle_detected",
    "senderId": "external-system-id",
    "transactionId": "ab347520-92a0-4807-af25-432002d3c329"
  },
  "Message": {
    "licensePlate": "DFM123",
    "countryCode": "FI",
    "side": "left"
  }
}
```
