# Vehicle management

The vehicle manager web application is the main interface for everything related to vehicle handling. New vehicles are added, and customers at specific energy companies are assigned to the vehicles here.

The vehicle learning itself is managed on the robot software, but the vehicle manager provides the rest of the needed data.

All operators will get an invitation to the vehicle manager by email.

The most recent version is available for the production environment at http://vehiclemanager.autofuel.racing/ and for the test environment, at http://test.current.vehiclemanager.autofuel.racing/.

A specific version of the client for the test environment is available at available at http://test.v0.3.0-beta1.vehiclemanager.autofuel.racing/. (Example)

## Log in to vehicle manager

Use the credentials supplied in your welcome email. First time, you will be prompted to change your password.

![01-vehicle-manager-login.png](images/learn-vehicle-manager/01-vehicle-manager-login.png)

## Learn vehicle

### First step

From the navigation bar, click `Learn vehicle` and then `Start`.

![02-vehicle-manager-learnvehicle.png](images/learn-vehicle-manager/02-vehicle-manager-learnvehicle.png)

### Enter VIN

Enter the VIN of the vehicle to be learned.
![03-vehicle-manager-enter-vin.png](images/learn-vehicle-manager/03-vehicle-manager-enter-vin.png)

### Add or edit vehicle data

If the vehicle already exists, you can edit the data, otherwise, you can add the data.

![04-vehicle-manager-edit-vehicle.png](images/learn-vehicle-manager/04-vehicle-manager-edit-vehicle.png)

The system will suggest existing brand, suggest existing models from the chosen brand, and suggest existing years and body types from the chosen model.
![05-vehicle-manager-add-vehicle.png](images/learn-vehicle-manager/05-vehicle-manager-add-vehicle.png)

If a value is not yet existing, you can enter it manually and choose `Create "..."`
![06-vehicle-manager-create-year](images/learn-vehicle-manager/06-vehicle-manager-create-year.png)

### Select reference

The system will find vehicles with similar specifications, and suggest the 5 best matches, from which you can pick a reference vehicle to speed up the learning process.
![07-vehicle-manager-select-reference](images/learn-vehicle-manager/07-vehicle-manager-select-reference.png)

### Select Robot

You can choose among the robots defined for the selected Energy Company, and choose the side of the robot (opposite of the fuel inlet side.)
![08-vehicle-manager-select-robot.png](images/learn-vehicle-manager/08-vehicle-manager-select-robot.png)

### Confirm

You will se an overview of the operation. Before confirming, make sure that the physical robot is ready, or - if testing - that the test bench is active and that the robot with the selected ID is in an `idle` state.
![09-vehicle-manager-learn-confirm.png](images/learn-vehicle-manager/09-vehicle-manager-learn-confirm.png)

### Testbench - robot idle

Make sure that the robot chosen in the Vehicle Manager is ready
![01-testbench-robot-ready](images/learn-testbench/01-testbench-robot-ready.png)

### Learning status

Back in vehicle manager, click `Confirm` and the system will now wait for a response from the robot.
![10-vehicle-manager-waiting.png](images/learn-vehicle-manager/10-vehicle-manager-waiting.png)
As soon as the robot enters learning-mode, it will respond and the status will update. Keep the window open, do not click any of the buttons yet.
![11-vehicle-manager-accepted.png](images/learn-vehicle-manager/11-vehicle-manager-accepted.png)

### Testbench - robot learning

The robot is now in learning mode. Notice that the robot has received the ID if the newly created vehicle, the chosen side and the configuration of the reference vehicle.
![02-testbench-robot-learning.png](images/learn-testbench/02-testbench-robot-learning.png)

### Testbench - complete learning

To complete the learning, we pretend that the robot has now recorded a new configuration for the vehicle, and scanned an RFID chip mounted in the car. For testing, we can input random strings as RFID code and robot configuration. Otherwise, enter a "real" RFID code and robot configuration in the fields.
![03-testbench-robot-learning-enter-data.png](images/learn-testbench/03-testbench-robot-learning-enter-data.png)

The robot will now send the learned data back to the vehicle manager.
![04-testbench-robot-learning-finished.png](images/learn-testbench/04-testbench-robot-learning-finished.png)

### Learning status

Back in vehicle manager, it will tell us that the learning process has completed.
![12-vehicle-manager-completed.png](images/learn-vehicle-manager/12-vehicle-manager-completed.png)

Now it's time to assign a customer to the learned vehicle, so click `Assign a customer`.

## Assign customer

The system will remember the VIN of the recently learned vehicle, needed in the first step of the customer assignment flow.
![13-vehicle-manager-assign-customer](images/learn-vehicle-manager/13-vehicle-manager-assign-customer.png)

Enter the customer data in the fields needed for the specific energy company.
![14-vehicle-manager-assign-customer-data](images/learn-vehicle-manager/14-vehicle-manager-assign-customer-data.png)

After confirming the customer data, the system will tell us that the data has been successfully saved.
![15-vehicle-manager-assign-customer-success](images/learn-vehicle-manager/15-vehicle-manager-assign-customer-success.png)

## Testbench - verify the vehicle

### Enter RFID and get configuration

In the testbench, take the RFID of the recently learned vehicle.
![05-testbench-robot-learning-rfid.png](images/learn-testbench/05-testbench-robot-learning-rfid.png)

Type it in the RFID field and select the proper side of the robot.
![06-testbench-robot-learning-enter-rfid-side](images/learn-testbench/06-testbench-robot-learning-enter-rfid-side.png)

Watch the robot receive the vehicle configuration that it just learned.
![07-testbench-robot-received-configuration](images/learn-testbench/07-testbench-robot-received-configuration.png)

### Receive customer data

Make sure that the dispenser associated with the chosen robot is idle

![08-testbench-dispenser-idle](images/learn-testbench/08-testbench-dispenser-idle.png)

Then, switch to the robot, choose `Vehicle parked` at the current transaction, and switch back to the dispenser again.

You will now see that the dispenser receives the customer details entered into the vehicle manager.

![09-testbench-dispenser-approval](images/learn-testbench/09-testbench-dispenser-approval.png)
