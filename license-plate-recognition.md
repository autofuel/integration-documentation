# License plate recognition

The system consists of two IP cameras (left/right lane) which are both running an on-camera app for recognizing license plates and they report any scanned license plate to a webhook (HTTP POST).

The format of the webhook message is vendor specific, so in order to keep the rest of the system generalized, the requests are handled by the `aplr-receiver` service running on the PC. This service interprets the webhook requests from the cameras and broadcasts an MQTT message with the license plate.

Since the LPR cameras are scanning for license plates several times per second, they might push multiple updates to the scanning results within a short time. Therefore, the `aplr-receiver` service will wait a little for subsequent updates to the detected license plate, before making a conclusion. Every time a license plate update is received, a message with the `isRecognizing: true` flag is broadcasted. This can be used to inform the driver that a detection is in progress, and also show the detected license plate, before the conclusion.

When there has been no updates for a while, the last received message will be broadcasted again, with the `isConclusive: true` set, and that value should be the one that is looked up by the state machine.

The PC software should listen on the MQTT topics `licenseplate/left` and `licenseplate/right` . On these topics, messages will be received in a structure like this:

```ts
export interface LPRMessage {
  licensePlate: string;
  countryCode: ISO3166a2Country;
  confidence: number;
  timestamp: ISO8601Timestamp;
  originalCountryCode?: string; // Only set if countryCode has been "replaced"
  originalLicensePlate?: string; // Only set if licensePlate has been "replaced"
  vehicleId: string; // Unique ID of the specific car, generated by the LPR camera
  isConclusive: boolean; // true if the license plate is the last update in a series
  isRecognizing: boolean; // true if the service is still determining the best result, based on a series of updates from the LPR camera.

}
```

When the license plate message is received, it should be handled similar to a scanned RFID tag, i.e. sent to the cloud service, which will respond with the appropriate car data if the car is subscribed to the automatic refueling. See the - [Robot communication](robot.md) document for details of the cloud communication.

## Testing

For testing, a scan of a license plate can easily be tested by using an MQTT client like this:

### Topic:

 `licenseplate/left`

### Message:

3 events are received, with the same car ID, while the system is waiting for updates

```json
{
  "licensePlate": "YVL123",
  "countryCode": "CY",
  "confidence": 0.9321,
  "timestamp": "2023-01-13T14:58:43.476Z",
  "vehicleId": "143121",
  "isConclusive": false,
  "isRecognizing": true
}
```

```json
{
  "licensePlate": "YVL123",
  "countryCode": "FI",
  "confidence": 0.8321,
  "timestamp": "2023-01-13T14:58:43.501Z",
  "vehicleId": "143121",
  "isConclusive": false,
  "isRecognizing": true
}
```

```json
{
  "licensePlate": "VVL123",
  "countryCode": "FI",
  "confidence": 0.8011,
  "timestamp": "2023-01-13T14:58:43.533Z",
  "vehicleId": "143121",
  "isConclusive": false,
  "isRecognizing": true
}
```

After the timeout, a conclusive message is sent, based on the latest update.

```json
{
  "licensePlate": "VVL123",
  "countryCode": "FI",
  "confidence": 0.8011,
  "timestamp": "2023-01-13T14:58:43.533Z",
  "vehicleId": "143121",
  "isConclusive": true,
  "isRecognizing": false
}
```

## Message retention 

The service that broadcasts license plate events may set the MQTT messages to be retained, so any new client (like the learning app) can see the last message that was sent before it connected.

However, this requires that the state machine will check the age of the timestamp before initating a refefuelling.
