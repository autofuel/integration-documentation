# Message structure

Each message is made of a list of attributes, used to identify, categorize and filter the message, in addition to a message body, which may contain additional data relevant for the message type.

Each resource must publish events every time its status is changing, and retrieve commands from a queue, and execute these commands.

All calls to the AWS API should be done using the SDKs offered by Amazon, not by calling the service URLs directly.

# Publishing event messages

Each system should publish event messages every time its status changes. The messages are published to the [AWS Simple Notification Service](https://aws.amazon.com/sns/) (SNS).

We work with two topic names:

- `robot-event` where the robot will publish any events (status change, errors etc.)
- `dispenser-event` where the dispenser will publish any events (status change, errors etc.)

## Publishing to topics

To use the topics in the AWS API, a `TopicArn` value should be generated, like this:

`arn:aws:sns:<AWS_REGION>:<AWS_ACCOUNT_ID>:<TOPIC_NAME>`

Example:

`arn:aws:sns:eu-west-1:719432241830:robot-event`

The message published to a `TopicArn`, have a list of `MessageAttributes`, which is used for categorizing, filtering and interpreting the message,as well as a custom `Message` field, used for sending any additional data.

The `Message` is a string, must not be empty, and should contain a JSON data structure, encoded as a string. If no additional data is available, it should be an empty object like `"{}"`.

Read more about publising in the relevant API here:
https://docs.aws.amazon.com/sns/latest/api/API_Publish.html

# Receiving command messages

All inputs to systems are read from queues in the [AWS Simple Queue Service](https://aws.amazon.com/sqs/) (SQS). Each system has its own dedicated queue with commands etc. which will arrive in sequential order (FIFO) and each message will arrive exactly once. The system should poll the queue all the time, and should delete the received message as soon as it's successfully executed.

**A message should be completely executed and deleted before the next message is requested from the same queue.**

## Reading queue messages

Each queue is identified by a URL. This URL is not supposed to be called directly, but serves as a resource locator and is given as a paramter to the AWS SQS request. The structure is this:

`http://sqs.<AWS_REGION>.amazonaws.com/<AWS_ACCOUNT_ID>/<QUEUE_NAME>.fifo`

When accessing a queue, the function [AWS.SQS.GetQueueUrl](https://docs.aws.amazon.com/AWSSimpleQueueService/latest/APIReference/API_GetQueueUrl.html) should be used to generate the URL from the known queue name.

In this system, the queue name is derived from the `entityType` and the `entityId`, concatenated by `--`, like `robot--1234567.fifo`. The `.fifo` suffix indicates that the queue is a FIFO queue and should be a part of the name supplied to `GetQueueUrl`.

Example URL:

`https://sqs.eu-west-1.amazonaws.com/719432241830/robot--TEST_ROBOT_1.fifo`

Read more about receiving queue messages in the relevant API here:
https://docs.aws.amazon.com/AWSSimpleQueueService/latest/APIReference/API_ReceiveMessage.html

## Continously reading messages

Each receive request to the queue should only read 1 message. As soon as a request has returned, the received message should be processed and executed. If the response is empty (i.e. no messages in queue), a new receive request should be made, until a message is received.

The system should only receive messages when it's in a state where it's able to process and execute these messages.

The countinous polling for messages whould have the longest timeout possible, in order to save system resources. This is done by setting the parameter `WaitTimeSeconds: 20` in the request.

# General AWS details

## Credentials

In order to authenticate with the AWS API, the following credentials are needed:

- AWS Access Key ID
- AWS Access Key Secret
- AWS Region

The keys should be handled with care, and not be committed into the source code and pushed to code repos, but only exist in a configuration file on the robot controller or payment system servers.

Before doing any other operations on the AWS API, the API should be configured with these credentials.

## Account ID

The AWS account ID is used to generate the proper ARNs for topics etc. To get the account number, make a request to [AWS.STS.GetCallerIdentity](https://docs.aws.amazon.com/STS/latest/APIReference/API_GetCallerIdentity.html) after configuring AWS.

# Message Attributes

## `messageType`

The type of message. Possible values:

- `event` - Inform the system that something has happened on your resource, i.e. status update, error etc.
- `command` - Ask a resource to execute an action

## `entityType`

The type of resource, publishing or receiving the message. Possible values:

- `robot` - A robot mounted on a gas station
- `dispenser` - A single dispenser operated by a robot. Even though the payment system might run centrally, each dispenser is treated as its own logical entity.

Robots and dispensers publish events to shared topcis, but have their own individual queues for receiving commands.

## `entityId`

The ID of the resource. Can be any string matching `[A-Za-z0-9-_]+`

## `senderId`

The ID of the resource creating the message.

For event messages, this will contain the `entityType` and `entityId` concatenated by `--`. Example, a robot with ID `98756789` will set a `senderId` of `robot--98756789`.

For command messages, the it will be the ID of the service generating the command, example: `arn:aws:lambda:eu-west-1:719432241830:function:startRefueling`

## `transactionId`

The ID of the ongoing transaction. A transaction ID is generated by the robot when a new session is initiated by an arriving car with an RFID chip. Only set when a message belongs to an ongoing session.

## `value`

For `event` messages, this is the current state of the resource and can be any string.

For `command` messages, this is the name of the command to execute.

## `valueType`

For `event` messages, this is the type of the current state of the resource, and can be either `status`, `error` or `warning`
