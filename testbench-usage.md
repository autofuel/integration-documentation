# Testbench

## Access

The most recent version is available on the URL
http://testbench-current.autofuel.racing/robot

A specific version of the test bench app is available an a URL like this.
http://testbench-v0.2.1.autofuel.racing/

The `v0.2.1` part of the URL indicates the build version the app.

The same build version is shown in the title bar of the app.

## Connect

Use the AWS key, secret and region supplied by mail.

![testbench connection](images/testbench-connect.png)

## IDs of resources

A list of resource IDs is supplied in the mail, along with the user credentials. The dispenser and robot IDs are mapped, and only dispensers related to a given robot can be activated by this robot - using the left/right options.

### Important!

**Only ONE INSTANCE OF EACH ID should exist at a given time.**

This means, that if the test bench is connected and have a robot or dispenser with a given ID defined, this ID should not be defined in any other running test bench, robot, or payment system.

Having multiple systems acting as the same ID will cause unexpected results.

During development, the best practice is to dedicate specific resource IDs to specific developers or systems.

Also, do not have multiple browser windows of the same browser, with the test bench, open at the same time. If you need to watch multiple resources simultaneously, use the app in two different browsers and only define a given resource ID in one of the app instances (i.e. do not define the same robot ID in both browses at once).

## Usage

### Create resources

After connecting, go to `Robot` and define a robot, using a supplied robot ID. Set the robot in `idle` state by clicking `Ready`

![define robot](images/testbench-create-robot.gif)

Then go to `Payment system` and define one or two dispensers, related to the robot.

### Initiate a session

At the robot instance, enter a random `RFID` value, click `Vehicle arrived` for left or right side, and wait for the vehicle data to appear.

### Approve payment

Then click `Vehicle parked` and go to the relevant dispenser. It should now wait for activation. Click `Activate` to indicate that the dispenser has been released (i.e. payment is approved).

### Proceed

Go back to the robot and proceed through the process, or simulate any of the error scenarios.

### Finish

To simulate that the nozzle is put back, go to the dispenser and click `Finish`.

### Alternative flows

To simulate that the user has requested the refueling to stop, go to the dispenser and click `Abort refueling`. This will cause the robot to remove the nozzle and finish the process.

To simulate that a certain limit of price/amout has been reached, go to the dispenser and click `Limit reached`. This will cause the robot to remove the nozzle and finish the process.

![testbench refueling](images/testbench-refueling.gif)

## Simulating vehicle ID lookup behaviors

Currently, any value supplied in the `RFID` field for the robot will cause dummy vehicle data to be sent to the robot.

To simulate an error, use the following values:

- `invalid_id` - the robot will receive a vehicle recognition error
- `system_failure` - the robot will receive a system error (not related to the specific customer)
- `timeout` - the robot will never receive any data, use this for simulating a lookup timeout

## Troubleshooting

### Message console

Go to `Console` to see all messages sent and received by the resources defined in the app, or publish your own custom messages.

### Clear data

On the front page, there are buttons to clear all app data, in case the app goes into an invalid state.

### Empty queue

When experimenting and creating and removing resources in the app, there might be messages in the queue which cannot be handled in the current state when the resource is re-created. Queues live in the cloud and are not cleared when clearing app data.

To be sure that everyting is in the initial position, you can click the `Empty command queue` for each resource.
