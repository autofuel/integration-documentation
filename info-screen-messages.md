# Info screen messages

## General flags

These optional flags can be used on most slide-screens during the process.

```ts
{
    // Indicates that people should not enter/exit the car at the current time. This will show a warning to the driver.
    needsSteadyVehicle: boolean,
    // A number between 0 and 1 can be specified to show a progress bar in the bottom of the screen. 
    progress: number,
}
```
## User instruction

### `DRIVE_FORWARD`

_The user should drive forward towards the robot and scanner._

### `WAIT_BEFORE_OPENING_FLAP`

_Tells the user to wait before opening the flap_

Do not open your flap yet!

### `UNLOCK_FLAP`

_Tells the user to unlow the flap now_

### `CAR_POSITIONING`

_Shows the user instructions to position the car_

#### Data format

```ts
interface CarPositioningData {
  // Optional: Position shown on the screen. Units are arbitrary with a linear relationship
  // If no position data is available, this field should not be defined.
  currentPosition?: {
    x: number; // track of the car low is left, high is right
    y: number; // driving distance low is bottom, high is top
  };
  validRange: {
    // the range where it is possible to refuel the car
    min: {
      x: number;
      y: number;
    };
    max: {
      x: number;
      y: number;
    };
  };
  totalRange: {
    // the total range that can be measured by the equipment
    min: {
      x: number;
      y: number;
    };
    max: {
      x: number;
      y: number;
    };
  };
  isValid: boolean; // Whether the current offset is within the valid range
}
```

#### Example

```json
{
  "type": "USER_INSTRUCTION",
  "timestamp": "2019-02-31T16:12:51.000Z",
  "locale": "en-US",
  "value": "CAR_POSITIONING",
  "data": {
    "currentPosition": {
      "x": 1223.23,
      "y": 212.21
    },
    "validRange": {
      "min": {
        "x": 1000.5,
        "y": 112.21
      },
      "max": {
        "x": 1100.0,
        "y": 312.21
      }
    },
    "totalRange": {
      "min": {
        "x": 5000.5,
        "y": -12.21
      },
      "max": {
        "x": 2100.0,
        "y": 612.21
      }
    },
    "isValid": false
  },
  "progress": 0.2,
  "operationMode": "NORMAL",
  "systemState": "REFUELING",
}
```

### `STOP_ENGINE`

_Tells the user to stop the engine._

### Examples

```json
{
  "type": "USER_INSTRUCTION",
  "timestamp": "2019-02-31T16:12:51.000Z",
  "locale": "en-US",
  "value": "STOP_ENGINE",
  "progress": 0.9,
  "operationMode": "NORMAL",
  "systemState": "REFUELING",
}
```

```json
{
  "type": "USER_INSTRUCTION",
  "timestamp": "2019-02-31T16:12:51.000Z",
  "locale": "en-US",
  "value": "STOP_ENGINE",
  "data": {},
  "operationMode": "NORMAL",
  "systemState": "REFUELING",
}
```

## Status

### `SUCCESS`

_Shown after a successful refueling_

You can now leave. Goodbye!

### `REFUELING`

_Shown when the refueling is in progress_

Fuelling...

### `PAYMENT_APPROVED`

_Shown shortly after payment is approved_

### `PAYMENT_AWAITING_APPROVAL`

_Shown when waiting for payment approval_

Waiting for approval

### `INITIALIZED`

_Shown when the system is started_

System initialized. Not started

### `ROBOT_MOVING`

_Indicates that the robot is moving_

### `READY`

_Shown when the robot is idle and ready to serve customers_

Welcome

### `RECOVERY_SUCCESSFUL`

_Shown when the robot has recovered from an error and is soon ready to serve new customers_

System recovered. You can now try again or leave.

### `RECOVERY_STARTED`

_Shown when recovering from an error_

Process Stopped. Trying to recover from error...

### `DETECTING_VEHICLE`

_Shown when the vehicle is detected but the license plate is not finally determined._

Detecting car license plate... BAA123 / NL

#### Data format

```ts
export interface LookingUpData {
  licensePlate?: string; // optional - only if license plate is scanned
  countryCode?: string; // ISO3166-1 alpha 2 optional - only if license plate is scanned
}
```

#### Example

```json
{
  "timestamp": "2020-09-12T12:31:31.000Z",
  "locale": "en-US",
  "type": "STATUS",
  "value": "DETECTING_VEHICLE",
  "data": {
    "licensePlate": "IOY829",
    "countryCode": "CY"
  },
  "operationMode": "NORMAL",
  "systemState": "IDLE",
}
```
### `LOOKING_UP_VEHICLE`

_Shown when the system is looking up the scanned car_

Looking up your car: BAA123 / NL

#### Data format

```ts
export interface LookingUpData {
  licensePlate?: string; // optional - only if license plate is scanned
  countryCode?: string; // ISO3166-1 alpha 2 optional - only if license plate is scanned
}
```

#### Example

```json
{
  "timestamp": "2020-09-12T12:31:31.000Z",
  "locale": "en-US",
  "type": "STATUS",
  "value": "LOOKING_UP_VEHICLE",
  "data": {
    "licensePlate": "BAA123",
    "countryCode": "NL"
  },
  "operationMode": "NORMAL",
  "systemState": "REFUELING",
}
```

## Safety error

### `INTRUDER_DETECTED`

_Shown when scanners detect foreign objects_

### `EMERGENCY_STOP`

_Shown when emergency stop button is pushed_

## User error

### `POSITIONING_TIMEOUT`

_Shown when the car is not parked correctly within the timeout_

### `CAR_ANGLE_OUTSIDE_RANGE`

_Shown when the user is parking incorrectly_

The car is not parked straight enough

### `CAR_TOO_FAR_AWAY`

_Shown when the user is parking incorrectly_

Car too far away from robot. Park closer

### `CAR_TOO_CLOSE`

_Shown when the user is parking incorrectly_

Car too close to robot. Park further away

### `WRONG_SIDE`

_The user has parked in at wrong side of the dispenser/robot._

You have parked at the wrong side - try again.

## System error

### `PAYMENT_SYSTEM_ERROR`

_A general issue with the payment system_

### `SYSTEM_ERROR`

_Indicates that some part of the system did not work as expected_

### `RECOVERY_ERROR`

An error occurred from which the system cannot restart

### `LOOKUP_SYSTEM_FAILURE`

_There was an error looking up data from the cloud system_

Error occured

## Data error

### `PAYMENT_NOT_APPROVED`

_The payment details of the current user was not approved_

Your payment was not approved

### `VEHICLE_NOT_RECOGNIZED`

_Shown when the scanned car is not found in the database_

License plate XYZ123 was not recognized

#### Data format

```ts
interface NotRecognizedData {
  licensePlate?: string; // optional - only if license plate is scanned
  countryCode?: string; // ISO3166-1 alpha 2 optional - only if license plate is scanned
}
```

#### Example

```json
{
  "type": "DATA_ERROR",
  "timestamp": "2019-02-31T16:12:51.000Z",
  "locale": "en-US",
  "value": "VEHICLE_NOT_RECOGNIZED",
  "data": {
    "licensePlate": "XYZ123",
    "countryCode": "FI"
  },
  "operationMode": "NORMAL",
  "systemState": "ERROR",
}
```

### `NO_CUSTOMER_REGISTERED`

_No customer data about the car, was found at the current energy company._

Your car is not registered

### `FUELTYPE_UNAVAILABLE`

_The car requires a fuel type which is not available at this dispenser_

Fuel type not available
