# Info-screen

## Communication

The info screen is managed by a separate application (full screen web app) which communicates with the robot PC application using the [MQTT](http://mqtt.org/) protocol.

To minimize latency, the sender must use QoS level 0 (at most once), which avoids roundtrip delays when sending messages.

## System setup

### System 2:

The info screen computer will run the MQTT server and start the web-app automatically. The robot PC software needs to be configured with the IP address of the info screen computer, in order to send the MQTT messages to the right place.

### System 3:

The robot PC software will be connected to the MQTT server running on the PC, and the info screen computer/player's browser will connect to this MQTT server.

## Testing

### No info screen computer

In order to test the setup without the info screen computer, the web applicaton can be run in a browser (INSERT URL HERE) and the MQTT server can be run installing [Mosquitto](https://mosquitto.org/download/) on the same PC which is running the web app. The robot PC software should then be configured to use the IP address of the PC running the infoscreen web application and MQTT server.

### No robot PC software

The expected behavior of the info screen PC can be mocked by using the application [MQTT Explorer](http://mqtt-explorer.com/). Send messages as described in these docs, and watch the info screen application show the right screens.

### Testing on LED Screens

* Create an infoscreen URL with the following parameters added:
  + `mqtt_url=mqtt://<Server_IP>:9001`
  + `dark=1`
  + `topbar=1`
  + `side=RIGHT` or `side=LEFT`
  + `fixedfontsize=11pt`

* Example: http://infoscreen-led-screen-adaption.autofuel.racing/?mqtt_url=mqtt://192.168.140.240:9001&dark=1&fixedfontsize=11pt&topbar=1&side=RIGHT

* Open the URL in Google Chrome

* Open chrome DevTools (F12)
* Open the device toolbar
* Enter 192x256 resolution

Now you have the same view as what will be shown on the LED screens.

![robot states](images/infoscreen-led-testing.png)

## Message structure

See all messages in [info-screen-messages.md](info-screen-messages.md).

The MQTT messages should be published to the `infoscreen/status` topic and be in JSON format, and follow this structure:

```ts
enum InfoScreenMessageType {
  SYSTEM_ERROR = "SYSTEM_ERROR",
  USER_INSTRUCTION = "USER_INSTRUCTION",
  STATUS = "SYSTEM_ERRSTATUSOR",
  USER_ERROR = "USER_ERROR",
  SAFETY_ERROR = "SAFETY_ERROR",
}

enum InfoScreenMessageType {
  SYSTEM_ERROR = "SYSTEM_ERROR",
  USER_INSTRUCTION = "USER_INSTRUCTION",
  STATUS = "SYSTEM_ERRSTATUSOR",
  USER_ERROR = "USER_ERROR",
  SAFETY_ERROR = "SAFETY_ERROR",
}

enum OperationMode {
  NORMAL = 'NORMAL',
  MAINTENANCE = 'MAINTENANCE',
  LEARNING = 'LEARNING',
  TESTING = 'TESTING',
  DISABLED = 'DISABLED',
}

enum SystemState {
  IDLE = 'IDLE',
  REFUELING = 'REFUELING',
  LEARNING = 'LEARNING',
  ERROR = 'ERROR',
  RECOVERING = 'RECOVERING',
  DISABLED = 'DISABLED',
}

interface InfoScreenMessage {
  type: InfoScreenMessageType;
  timestamp: string; // ISO8601-2
  locale: string; // RFC5646 locale
  value: string; // See list of possible values for each type
  data?: any; // Optional set of data for each type
  progress?: number; // Optional value between 0 and 1 (1 means 100%)
  operationMode?: OperationMode;
  systemState?: SystemState;
}
```

Examples

```json
{
  "type": "STATUS",
  "timestamp": "2019-02-31T16:12:51.000Z",
  "locale": "en-US",
  "value": "READY",
  "operationMode": "NORMAL",
  "systemState": "IDLE",
}
```

```json
{
  "type": "USER_INSTRUCTION",
  "timestamp": "2019-02-31T16:12:51.000Z",
  "locale": "en-US",
  "value": "CAR_POSITIONING",
  "data": { "offset": 341, "isValid": false },
  "operationMode": "LEARNING",
  "systemState": "LEARNING",
}
```

```json
{
  "type": "USER_ERROR",
  "timestamp": "2019-02-31T16:12:51.000Z",
  "locale": "en-US",
  "value": "CAR_TOO_FAR_AWAY",
  "progress": 0.7,
  "operationMode": "NORMAL",
  "systemState": "ERROR",
}
```

## Locale

The locale specified in the message should be the selected locale of the current customer, and will be supplied along with the vehicle data needed for refueling. If no locale is specified in the vehicle data, it should default to `en-US` . For messages like the welcome-message, and for customers without a specified locale preference, a default locale of the current installation country can be hardcoded in the robot software settings, and be sent to the info screen.

## Message types

* `SYSTEM_ERROR`: The system made has technical error

* `USER_INSTRUCTION` : Instructions that the user should follow

* `STATUS`: The status of the robot, whether it's idle or busy doing a task

* `USER_ERROR`: The user is doing a mistake

* `SAFETY_ERROR`: The system stopped due to a safety alert

* `DATA_ERROR`: The avaliable data does not allow refueling

## Keep-alive

In order to avoid situations where the info display shows an outdated status (i.e. the robot has been shut off or software has crashed), the robot software should continously broadcast its status every 30th second.

If the info screen application has not received a status during the last 30 seconds or so, it will fall back and show a "Not Available" mode which is reflecting that the robot PC software is not ready to serve customers.

## Dual-side topics

(Available in version 0.1.0 and up)

On installations with dual screens for the left and right side, each side will be configured to be the left or right side, by an URL parameter to the web application like `side=LEFT` or `side=RIGHT` .

To address MQTT communication the left/right screen, or a shared screen, send to one of these topics:

* `infoscreen/TEST`,  `infoscreen/test` For testing only, will affect both left and right sides
* `infoscreen/SHARED`,  `infoscreen/status` For info screens where no side is set, i.e. shared screens.
* `infoscreen/LEFT` Left side
* `infoscreen/RIGHT` Right side

## Traffic-lights

(Available in version 0.1.0 and up)

The virtual on-screen traffic lights on left and right side can be controlled via MQTT messages as described1  below:

### Topics 

* `traffic-light/TEST` For testing only, will affect both left and right sides
* `traffic-light/LEFT` Left side
* `traffic-light/RIGHT` Right side
* 

### Data structure

```ts
export enum TrafficLightStatusType {
  RED = 'RED',
  GREEN = 'GREEN',
  OFF = 'OFF',
  BOTH = 'BOTH',
}

export type TrafficLightStatusMessage = {
  status: TrafficLightStatusType
}
```

### Example

Topic: `traffic-light/LEFT`

Message: 

```json
{
  "status": "RED"
}
```

This will make the left traffic light show red.
