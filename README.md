# Last updated 2022-07-12

# Autofuel integration

# Protocol

The parts of the Autofuel system communicate using the Amazon Web Services (AWS) infrastructure.

# Pages

- [Vehicle management](vehicle-management.md)
- [Message structure](message-structure.md)
- [Robot communication](robot.md)
- [Dispenser communication](dispenser.md)
- [Robot states](robot-states.md)
- [Refueling flow](refueling-flow.md)
- [Testbench usage](testbench-usage.md)
- [License plate recognition](license-plate-recognition.md)
- [Info screen](info-screen.md)

# Overview

![system overview](images/autofuel_infrastructure_overview.svg)

# Relevant parts

## Robot manager

The computer controlling the communication of each robot. One instance per robot, installed next to the robot controller electronics.

## Energy company payment system

The system at the energy company, which is responsible for communication with the dispensers and payment apps if relevant, and for validating customer/payment IDs.

## Vehicle manager

Handles the references, RFID tokens, refueling configurations and data for all vehicles across energy companies. One global instance for all Autofuel systems.

## Refuel manager

Handles the communication with the robot and the payment system at the energy company, and maps specific vehicles to specific customers. One instance for each energy company.
