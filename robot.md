# Robot

## Commands

The commands below are sent to the robot, from the refuel manager, and the robot should read these commands from its dedicated SQS queue.

`refuel` - Start refueling of the present vehicle

```json
{
  "fuelType": "diesel", // the product to be picked from the dispenser, based on either engine type or customer preference.
  "timeout": 123, // (Optional) The timeout in seconds, before the robot will end the refueling, if no nozzle clicks or stop commands are received. If let out, a default timeout should be applied by the robot software.
  "productPreferenceKeys": ["gasoline-95e10", "gasoline-98e5"], // (Optional) as list of keys of the products defined for the company in vehicle manager. The first product is the customer's top priority etc. The system should compare this list with the available products at the installed nozzles, and pick the first match.
}
```

`set_vehicle_data` - Set the data needed by the robot for refueling the vehicle. This command is sent to the robot as soon as the car has been looked up in the system.

```json
{
  "configuration": "<x>24</x><y>548</y><z>1250</z>", // the data needed by the robot, in order to handle the refueling. This data is generated during the learning steps.
  "licensePlate": "BP30963", // The license plate of the vehicle in the database
  "countryCode": "DK", // The country code (ISO3116-1 alpha2) of the vehicle in the database
  "localePreference": "en-US", // Optional - The preferred locale (IETF language tag) of the customer.
  "fuelType": "diesel", // The fuel type of the vehicle
  "isConfigurationVerified": true, // Indicates whether the current configuration version for the car, has already been used for a sucessful refueling cycle.
  "vehicleDetails": { // Details about the car, needed for learning.
    "model": "CX5",
    "brand": "Mazda",
    "year": 2007, // Encoded as a number
    "bodyType": "SUV"
  }}
```

`learn_vehicle` - The vehicle manager puts the robot into learning mode.

```json
{
  "vehicleId": "364567890-97865", // the ID of the vehicle that's just been created in the vehicle database, and is about to be learned. This is used as a reference when uploading the learning results.
  "side": "right", // which side of the robot to operate on, 'left' or 'right'.
  "referenceConfiguration": "<x>24</x><y>548</y><z>1250</z>", // (Optional) the configuration of a similar vehicle which is used as a reference for learning this particular vehicle. If not supplied, the learning will start from scratch.
  "licensePlate": "BP30963", // The license plate of the vehicle in the database
  "countryCode": "DK" // The country code (ISO3116-1 alpha2) of the vehicle in the database
}
```

* `stop` - Stop refueling. This command tells the robot to stop the refueling and put the nozzle back, close flap etc. This will ususally be sent if the dispenser reports that a price limit has been reached, or if the customer somehow ends the refueling (not by emergency stop).
* `set_lookup_vehicle_not_recognized` - The vehicle was not recognized in the database
* `set_lookup_system_failure` - The vehicle database has a technical error.
* `set_wrong_side` - the vehicle is parked on the wrong side of the robot
* `set_no_customer_registered` - no customer is registered for the vehicle, at the current oil company
* `set_fueltype_unavailable` - the fuel type need for the vehicle is not available on the dispenser
* `set_payment_customer_failure` - the payment system could not authorize the specified customer
* `set_payment_system_failure` - the payment system could not authorize the specified customer

The `Message` content must be a JSON-object containing all the data that is found relevant for each case - in addition to any required fields listed for a specific event. For the commands mentioned above, which sets error messsages, the message content will have a localized error message in a field named `localizedUserMessage` .

## Events

Events are sent from the robot, in order to inform the rest of the system about any event that occur at the robot.

All events should be published to the topic named `robot-event` , for example:

```json
TopicArn: "arn:aws:sns:eu-west-1:719432241830:robot-event"
```

The `Message` content must be a JSON-object containing all the data that is found relevant for each case - in addition to any required fields listed for a specific event.

### Status

All of these events have the `valueType` set to `status` .

#### General for robot system

These events are for monitoring the general status of the robot, and not related to any specific transaction ID, and therefore has no `transactionId` attribute.

* `starting` - the systems of the robot are booting and things are intializing.
* `shutting_down` - the robot system is shutting down
* `idle` - the robot is idle and ready for new tasks

#### Session specific (refueling)

These events are related to a specific refueling session at the robot, and must all contain the `transactionId` attribute, which is initially generated by the robot.

* `awaiting_vehicle_data` - should be sent by the robot as soon as the license plate is scanned, i.e. the car is arriving at the robot. In this step, the robot should generate a random, uniqe `transactionId` which will be used in all further communication of the refueling session.

Example:

License plate

```json
{
  "side": "left", // the side of the robot, where the car has arrived. 'left' or 'right'.
  "licensePlate": "BP30963", 
  "countryCode": "DK" //  ISO 3166 Alpha 2
}
```

* `awaiting_vehicle_parking` - sent when the vehicle data is received and the vehicle should park.
* `vehicle_parked` - sent as soon as the car is in parked correctly. This message will trigger the payment approval, and start the refueling process at the dispenser.
* `awaiting_dispenser` - sent immediately after `vehicle_parked`, indicating that the robot is waiting for the dispenser to activate. When the robot receives a `refuel` command, the dispenser has been activated.
* `preparing` - opening the door of the robot etc. triggered by a `refuel` command.
* `finding_fuel_door`
* `opening_fuel_door`
* `grabbing_nozzle`
* `inserting_nozzle`
* `refueling`
* `nozzle_shut_off` - when the nozzle click is activated
* `stopped` - the robot has been told to stop and put nozzle back, caused by limit reached or user interaction
* `removing_nozzle`
* `replacing_nozzle`
* `closing_fuel_door`
* `done` - the refueling is done, the car can proceed
* `finishing` - moving robotic arm in parked position and closing the door

#### Session specific (learning)

These events are related to a learning session at the robot, and must contain the `transactionId` attribute, which is generated by the backend and received with the `learn_vehicle` command.

* `learning` - the robot has been put into learning mode, and can learn a new vehicle using the robot controller software. Triggered by `learn_vehicle` command.

```json
{
  "vehicleId": "364567890-97865", // the ID supplied when the robot received the `learn_vehicle` command.
  "referenceConfiguration": "<x>24</x><y>548</y><z>1250</z>" // (Optional) The reference configuration used for the learning.
}
```

* `learned_vehicle` - the robot has learned a new vehicle. .

```json
{
   "configurationVersion": "wsexdrf6tg7yh8uj9", // Version ID of the file saved in S3.
   "side": "left" // side of the robot, `left` or `right``
}
```

#### Initiate learning from the robot/system software

If a learning session should be initiated by the system itself, (using PC software or Learning App) and not through the Vehicle Manager user interface, this event can be used.
* `requesting_learning` - makes the vehicle manager look up the specified car based on license plate, gets current configuration if any, and initiates a learning session based on this, so the system receives a `learn_vehicle` command. 

```json
{
  "side": "left",
  "licensePlate": "AFG531",
  "countryCode":"FI" 
}
```

### Warning

All of these events have the `valueType` set to `warning` .

#### Session specific

These events are related to a specific refueling/learning session at the robot, and must all contain the `transactionId` attribute.

* `interrupted_by_user` - the process is interrupted by user (e.g. stop button or similar)
* `interrupted_by_sensor` - the process is interrupted by a safety sensor
* `cannot_recognize_vehicle` - the vehicle's License plate is not in the database
* `lookup_system_failure` - the vehicle database system is down
* `vehicle_lookup_timeout` - no reply from the vehicle database
* `payment_timeout` - no refuel command received within a certain time frame, so the robot will go back to idle mode and abort the session
* `parking_timeout` - the parking has not completed within a certain time frame, so the robot will go back to idle mode and abort the session
* `learning_aborted` - the learning process was aborted
* `wrong_side` - the vehicle is parked on the wrong side of the robot
* `no_customer_registered` - no customer is registered for the vehicle, at the current oil company
* `fueltype_unavailable` - the fuel type need for the vehicle is not available on the dispenser
* `payment_customer_failure` - the payment system could not authorize the specified customer
* `payment_system_failure` - the payment system has a general failure

### Error

All of these events have the `valueType` set to `error` .

#### General for robot

* `cannot_initialize` - the robot cannot initialize
* `internal_error` - some internal error has happened
* `robot_not_found` - attempt to do operation on non existing robot.
* `robot_busy` - attempt to create new robot session on a non-idle robot.

#### Session specific

These events are related to a specific refueling/learning session at the robot, and must all contain the `transactionId` attribute.

* `cannot_prepare` - cannot open the doors of the cabinet
* `cannot_find_fuel_door` - cannot locate the fuel inlet (vision/touch etc)
* `cannot_open_fuel_door` - cannot open the flap
* `cannot_grab_nozzle` - cannot grab the nozzle from the dispenser
* `cannot_find_hole` - cannot locate the hole (vision)
* `cannot_insert_nozzle` - cannot insert the nozzle
* `cannot_activate_nozzle` - cannot activate the nozzle
* `cannot_remove_nozzle` - cannot remove the nozzle
* `cannot_replace_nozzle` - cannot put the nozzle back in the dispenser
* `cannot_close_fuel_door` - cannot close the flap
* `cannot_finish` - cannot close the cabinet doors
* `session_internal_error` - other internal errors not related to a specific step
* `session_not_found` - attempt to do operation on non existing robot session. Triggered if a robot receives a command which has a `transactionId` which is not known by the robot.
* `session_not_current` - attempt to do operation on robot session which is not the current/active one. triggered when a command with a `transactionId` which is not the current one, but is previously known by the robot, is received.
* `session_already_initialized` - attempt to add initial entry to an existing robot session
* `not_supported_in_current_state` - attempt to do operation in wrong state of robot session.
* `session_has_ended` - attempt to execute command on an ended robot session

## Usage

During the car's arrival at the robot, the robot will detect the license plate and will provide the license plate, country code and the side of the vehicle (left/right) as data when sending the `awaiting_vehicle_data` event.

When receiving the `set_vehicle_data` command, the robot should know the dimensions of the vehicle and start guiding the driver to park properly.

When the Autofuel refuel manager gets a message from the energy company, saying that the dispenser has been activated, it will tell the robot to start the refueling by sending a `refuel` command to the robot's queue. with the relevant data in the body.

The robot will continously provide its status during the refueling process, by sending `status` , `error` and `warning` events to the `robot-event` topic.

## Examples

### Car has arrived

Sent to topic: `robot-event`

```json
{
  "MessageAttributes": {
    "entityId": "TEST_ROBOT_1",
    "entityType": "robot",
    "messageType": "event",
    "valueType": "status",
    "value": "awaiting_vehicle_data",
    "senderId": "robot--TEST_ROBOT_1",
    "transactionId": "ab347520-92a0-4807-af25-432002d3c329"
  },
  "Message": {
    "licensePlate": "DFM123",
    "countryCode": "FI",
    "side": "left"
  }
}
```

### Robot is ready and car is parked

Sent to topic: `robot-event`

```json
{
  "MessageAttributes": {
    "entityId": "TEST_ROBOT_1",
    "entityType": "robot",
    "messageType": "event",
    "valueType": "status",
    "value": "awaiting_dispenser",
    "senderId": "robot--TEST_ROBOT_1",
    "transactionId": "ab347520-92a0-4807-af25-432002d3c329"
  },
  "Message": {}
}
```

### Request robot to start refueling

Received in queue: `robot--TEST_ROBOT_1`

```json
{
  "MessageAttributes": {
    "messageType": "command",
    "entityType": "robot",
    "entityId": "TEST_ROBOT_1",
    "value": "refuel",
    "senderId": "arn:aws:lambda:eu-west-1:719432241830:function:startRefueling",
    "transactionId": "ab347520-92a0-4807-af25-432002d3c329"
  },
  "Message": {
    "fuelType": "diesel",
    "timeout": 120,
    "productPreferenceKeys": ["super-diesel-b7", "diesel-b7"]
  }
}
```
